//Cat Array
let cats = [
  {
    name: "Tuna",
    breed: "Siamese",
    gender: "Female",
    neutered: true,
  },
  {
    name: "Chester",
    breed: "Tabby",
    gender: "Male",
    neutered: false,
  },
  {
    name: "Blue",
    breed: "Naked",
    gender: "Female",
    neutered: false,
  },
];

//TODO: Make a function to add a new cat into the array
// Name the function addNewCat

//1. Make a cat object
//2. Put cat object into a function
// 3. change the hard coded cat variables and call the function
//4. push to cats array



// First function

// const addNewCat = () => {
//   let newCat = { 
//     name: "Daisy",
//     breed: "Tabby",
//     gender: "Female",
//     neutered: true,
//   };
  
//   cats.push(newCat);
//   console.log(cats);
// }

// addNewCat();


console.log('\n===\n');
// Changing variables to add new cats

const addNewCat = (catName, catBreed, catGender, catNeutered) => {
  let newCat = { 
    name: catName,
    breed: catBreed,
    gender: catGender,
    neutered: catNeutered,
  };
  
  cats.push(newCat);
  console.log(cats);
}

addNewCat('daisy', 'tabby', 'female', true);
addNewCat('muffins', 'striped', 'female', false);
addNewCat('fluffy', 'long haired', 'female', true);